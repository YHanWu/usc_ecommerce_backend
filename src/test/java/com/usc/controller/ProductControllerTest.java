package com.usc.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.Matchers.*;

import org.assertj.core.api.Assertions;
import org.assertj.core.error.ShouldHaveSameSizeAs;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.usc.beans.Product;
import com.usc.eCommerce.ECommerceApplication;
import com.usc.repository.ProductRepository;
import com.usc.serviceImpl.ItemServiceImpl;
import com.usc.serviceImpl.ProductServiceImpl;

import ch.qos.logback.core.status.Status;


//@WebMvcTest
//@ContextConfiguration(classes = ProductController.class)
// https://stackoverflow.com/questions/56712707/springboottest-vs-contextconfiguration-vs-import-in-spring-boot-unit-test


//
//@SpringBootTest(classes = ECommerceApplication.class)
//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
class ProductControllerTest {

//	@MockBean
//	private ProductServiceImpl productServiceImpl;
//	
//	@MockBean
//	private ItemServiceImpl itemServiceImpl;
//	
//	@MockBean
//	private ProductRepository productRepository;
//	
//	@Autowired
//    private MockMvc mockMvc;
//    
//	
//	//(Integer id, String item, String brand, String category, String country, String type, String size, String region, String status, String url, String price, String taste)
//	
//	Product product1 = new Product(1, "testItem1", "testBrand", "testCategory", "testCountry", "testType", "testSize", "testRegion", "testStatus", "testUrl", "testPrice", "testTaste");
//	Product product2 = new Product(2, "testItem2", "testBrand", "testCategory", "testCountry", "testType", "testSize", "testRegion", "testStatus", "testUrl", "testPrice", "testTaste");
//	Product product3 = new Product(3, "testItem3", "testBrand", "testCategory", "testCountry", "testType", "testSize", "testRegion", "testStatus", "testUrl", "testPrice", "testTaste");
//	@Test
//    public void testNotNull() {
//        Assertions.assertThat(productController).isNotNull();
//    }
	
	
	
//	//Verify HTTP Request Mapping And Deserialization
//	@WithMockUser("spring")
//	@Test
//	void testGetProduct_success() throws Exception{
//		List<Product> products = new ArrayList<>(Arrays.asList(product1, product2, product3));
//		Mockito.when(productRepository.findAll()).thenReturn(products);
//		this.mockMvc.perform(MockMvcRequestBuilders.get("/product"))
//			.andDo(MockMvcResultHandlers.print())
//			.andExpect(MockMvcResultMatchers.status().isOk());
//		
//		this.mockMvc.perform(MockMvcRequestBuilders.get("/product").contentType(MediaType.APPLICATION_JSON))
//			.andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
//			.andExpect(MockMvcResultMatchers.jsonPath("$[0].item", is("testItem1")));
//	}
	
	
	//Verify Field Validation
//	@WithMockUser("spring")
//	@Test
//	void testUpdateProduct_FailWhenNotFound() throws Exception{
//		this.mockMvc.perform(MockMvcRequestBuilders.put("/edit").contentType(MediaType.APPLICATION_JSON)
//			.content(" {}"))
//			.andDo(MockMvcResultHandlers.print())
//			.andExpect(MockMvcResultMatchers.status().isNotFound());
//		
//	}

	
	

}
