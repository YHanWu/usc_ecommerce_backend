package com.usc.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.usc.beans.Order;
import com.usc.beans.User;
import com.usc.eCommerce.ECommerceApplication;
import com.usc.repository.UserRepository;
import com.usc.service.UserService;

@SpringBootTest(classes = ECommerceApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class UserControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	PasswordEncoder passwordEncoder;
	
	@MockBean
	private UserRepository userRepository;
	
	
//	@PostMapping
//	public Response addUser(@RequestBody User user) {
//		//要求接收 json format
//		return userService.register(user);
//	}
	User user1 = new User(1, "testUser1", "testPassword1");
	User user2 = new User(2, "testUser2", "testPassword2");
	User user3 = new User(3, "testUser3", "testPassword3");
	
	@WithMockUser(username="spring", authorities ="ROLE_ADMIN")
	@Test
	void testGetAllUsers() throws Exception{
		List<User> users = new ArrayList<User>(Arrays.asList(user1, user2, user3));
		Mockito.when(userRepository.findAll()).thenReturn(users);
		this.mockMvc.perform(MockMvcRequestBuilders.get("/users/users"))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().isOk());
		
		
		this.mockMvc.perform(MockMvcRequestBuilders.get("/users/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].username", is("testUser1")))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].id", is(1)));
	}
	
	@WithMockUser(username="spring", authorities ="ROLE_ADMIN")
	@Test
	void getUserById_success() throws Exception{
		Optional<User> userIdTest = Optional.of(new User(1, "user1", "password"));
		Mockito.when(userService.getUserById(1)).thenReturn(userIdTest);
		this.mockMvc.perform(MockMvcRequestBuilders.get("/users/user/{id}", 1)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$.password", is("password")))
		.andExpect(MockMvcResultMatchers.jsonPath("$.username", is("user1")))
		.andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)));;
		
	}
	
	@WithMockUser(username="spring", authorities ="ROLE_ADMIN")
	@Test
	void getUserById_FailWithoutPathVariable() throws Exception{
		Optional<User> userIdTest = Optional.of(new User(1, "user1", "password"));
		Mockito.when(userService.getUserById(1)).thenReturn(userIdTest);
		this.mockMvc.perform(MockMvcRequestBuilders.get("/users/user/{id}", "")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());
		
	}
	
	@WithMockUser(username="spring", authorities ="ROLE_ADMIN")
	@Test
	void getOrdersByUser_Success() throws Exception{
		Optional<User> userIdTest = Optional.of(new User(1, "user1", "password"));
		Order order = new Order(1, 140.00, "test address", user1);
		userIdTest.get().setOrder(Arrays.asList(order));
		Mockito.when(userService.getUserById(1)).thenReturn(userIdTest);
		this.mockMvc.perform(MockMvcRequestBuilders.get("/users/user/{id}/orders", 1)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].money", is(140.00)))
		.andExpect(MockMvcResultMatchers.jsonPath("$[0].address", is("test address")));

	}

}
