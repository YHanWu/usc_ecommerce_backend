package com.usc.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.usc.beans.Product;


@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{
	
	//List<Product> findByNameContaining(String name, Pageable pageable);
	Product findByItem(String item);

}
