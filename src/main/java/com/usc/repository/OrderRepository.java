package com.usc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.usc.beans.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{
	
	List<Order> findOrdersByMoney(double money);
	
	List<Order> findOrdersByUserId(int id);

}
