package com.usc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.usc.beans.Item;
import com.usc.beans.User;
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer>{
	
	//Item findByName(String name);

}
