package com.usc.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.usc.beans.User;
import com.usc.beans.UserRole;

public class CustomUserDetails implements UserDetails {
	
	private User user;
	public CustomUserDetails(User user) {
		this.user = user;
	}
	
	public boolean hasRole(String roleName) {
		return this.user.hasRole(roleName);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<UserRole> roles = user.getUserRole();
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		
		for(UserRole role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getType()));
		}
		
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

}
