package com.usc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.OrderItem;
import com.usc.serviceImpl.OrderItemServiceImpl;

@RestController
public class OrderItemController {
	
	private OrderItemServiceImpl orderItemServiceImpl;
	
	@GetMapping("/orderItems")
	public List<OrderItem> getAllOrderItems(){
		return orderItemServiceImpl.getAllOrderItems();
	}
	
	@GetMapping("/orderItem/{id}")
	public Optional<OrderItem> getOrderItemById(@PathVariable Integer id) {
		return orderItemServiceImpl.getOrderItemById(id);
	}

}
