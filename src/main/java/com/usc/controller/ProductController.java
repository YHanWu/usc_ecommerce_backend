package com.usc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Product;
import com.usc.beans.Response;
import com.usc.repository.ProductRepository;
import com.usc.serviceImpl.ItemServiceImpl;
import com.usc.serviceImpl.ProductServiceImpl;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductServiceImpl productServiceImpl;
	
	@Autowired
	private ItemServiceImpl itemServiceImpl;
	
	@Autowired
	private ProductRepository productRepository;

	@GetMapping
	public ResponseEntity<List<Product>> getProduct(
			@RequestParam(defaultValue = "id") String name,
			@RequestParam(defaultValue = "0") Integer page, 
			@RequestParam(defaultValue = "8") Integer size) {
		List<Product> list = productServiceImpl.getProduct(name, page, size);

		return new ResponseEntity<List<Product>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
//	@GetMapping
//	public List<Product> getProducts(){
//		return (List<Product>) productRepository.findAll();
//		
//	}
	
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@PostMapping
	public void addProduct(@RequestBody Product product) {		
		
		productServiceImpl.AddProduct(product);
		itemServiceImpl.AddItem(product);
		
	}
	
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@PutMapping("/edit")
	public Response updateProduct(@RequestBody Product product) {
		productServiceImpl.updateProduct(product);
		itemServiceImpl.updateItem(product);
		
		return new Response(true, "product update");
	}
	
	@DeleteMapping("/product/{id}")
	public void deleteProduct(@PathVariable Integer id) {
		productServiceImpl.deleteProduct(id);
	}

	// @GetMapping("/getByName")

}
