package com.usc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
import com.usc.beans.Response;
import com.usc.beans.User;
import com.usc.repository.UserRepository;
import com.usc.service.UserService;


@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserService userService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;
	
	
	
	
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	@GetMapping("user/{id}")
	public Optional<User> getUserById(@PathVariable Integer id) {
		return userService.getUserById(id);
	}
	
	
	@GetMapping("user/{id}/orders")
	public List<Order> getOrdersByUser(@PathVariable Integer id){
		Optional<User> user = userService.getUserById(id);
		if(user.isPresent()) {
			User newUser = user.get();
			return newUser.getOrder();
		}
		return null;
	}
	
	
	@PostMapping
	public Response addUser(@RequestBody User user) {
		//要求接收 json format
		return userService.registerAdm(user);
	}
	
	


}
