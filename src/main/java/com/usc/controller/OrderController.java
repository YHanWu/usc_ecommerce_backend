package com.usc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
import com.usc.beans.OrderItem;
import com.usc.beans.User;
import com.usc.repository.OrderRepository;
import com.usc.repository.UserRepository;
import com.usc.service.UserService;
import com.usc.serviceImpl.OrderServiceImpl;



@RestController
public class OrderController {
	
	@Autowired
	private OrderServiceImpl orderServiceImpl;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	
	@Autowired
	private UserService userService;
	
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/orders")
	public List<Order> getAllOrders(){
		return orderServiceImpl.getAllOrders();
	}
	
	@GetMapping("/order/{id}")
	public Optional<Order> getOrderById(@PathVariable Integer id) {
		return orderServiceImpl.getOrderById(id);
	}
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/order/{id}/orderItems")
	public List<Order> getOrdersByUser(@PathVariable Integer id){
		//List<Order> orders =  orderRepository.findOrdersByMoney(id);
		List<Order> orders =  orderRepository.findOrdersByUserId(id);
		//System.out.println(orders.size());
		return orders;
	}
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@GetMapping("/order/orderItems")
	public List<Order> getOrders(Authentication authentication){
		//List<Order> orders =  orderRepository.findOrdersByMoney(id);
		List<Order> orders =  orderRepository.findOrdersByUserId(userRepository.findByUsername(authentication.getName()).getId());
		//System.out.println(orders.size());
		return orders;
	}
	

	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PostMapping("order/create")
	public Order orderCreate(@RequestBody Order order, Authentication authentication) {
		System.out.println(order.toString());
		return orderServiceImpl.orderCreate(order, authentication);
	}

}
