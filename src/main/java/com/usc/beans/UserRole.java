package com.usc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "user_role")
public class UserRole implements GrantedAuthority {

	private static final long serialVersionUID = 1L;
	@Id
	private Integer id;


	@Column
	private String type;

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public UserRole(Integer id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRole(int id) {
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "UserRole [id=" + id + ", type=" + type + "]";
	}

	

}
