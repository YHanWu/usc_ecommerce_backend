package com.usc.beans;

import java.util.Map;

import org.springframework.http.HttpStatus;

//My uniform response


public class HttpResponse {
	
	protected String timeStamp;
	protected int statusCode;
	protected HttpStatus status;
	protected String message;
	protected Map<?, ?> date;
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<?, ?> getDate() {
		return date;
	}
	public void setDate(Map<?, ?> date) {
		this.date = date;
	}
	public HttpResponse(String timeStamp, int statusCode, HttpStatus status, String message, Map<?, ?> date) {
		super();
		this.timeStamp = timeStamp;
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.date = date;
	}
	public HttpResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "HttpResponse [timeStamp=" + timeStamp + ", statusCode=" + statusCode + ", status=" + status
				+ ", message=" + message + ", date=" + date + "]";
	}
	
	

}
