package com.usc.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "item_detail")
public class Item {

	@Id
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "brand")
	private String brand;
	@Column(name = "category")
	private String category;
	@Column(name = "country")
	private String country;
	@Column(name = "type")
	private String type;
	@Column(name = "size")
	private String size;
	@Column(name = "region")
	private String region;
	@Column(name = "status")
	private String status;
	@Column(name = "url")
	private String url;
	@Column(name = "price")
	private String price;
	@Column(name = "Taste")
	private String Taste;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTaste() {
		return Taste;
	}

	public void setTaste(String taste) {
		this.Taste = taste;
	}
	
	public Item(Integer id, String name, String brand){
		super();
		this.id = id;
		this.name = name;
		this.brand = brand;
			}



	public Item(Integer id, String name, String brand, String category, String country, String type, String size,
			String region, String status, String url, String price, String taste, List<OrderItem> orderItems) {
		super();
		this.id = id;
		this.name = name;
		this.brand = brand;
		this.category = category;
		this.country = country;
		this.type = type;
		this.size = size;
		this.region = region;
		this.status = status;
		this.url = url;
		this.price = price;
		this.Taste = taste;

	}

	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", brand=" + brand + ", category=" + category + ", country="
				+ country + ", type=" + type + ", size=" + size + ", region=" + region + ", status=" + status + ", url="
				+ url + ", price=" + price + ", Taste=" + Taste  + "]";
	}

}
