package com.usc.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "order_detail")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private double money;
	private String address;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User user;

	@OneToMany(mappedBy = "order")
	private List<OrderItem> orderitems;

	@Override
	public String toString() {
		return "Order [id=" + id + ", money=" + money + ", address=" + address + ", user=" + user + ", orderitems="
				+ orderitems + "]";
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(Integer id, double money, String address, User user, List<OrderItem> orderitems) {
		super();
		this.id = id;
		this.money = money;
		this.address = address;
		this.user = user;
		this.orderitems = orderitems;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public User getUser() {
		return user;
	}

	
	public void setUser(User user) {
		this.user = user;
	}


	public List<OrderItem> getOrderitems() {
		return orderitems;
	}

	public void setOrderitems(List<OrderItem> orderitems) {
		this.orderitems = orderitems;
	}
	
	public Order(Integer id, double money, String address, User user) {
		super();
		this.id = id;
		this.money = money;
		this.address = address;
		this.user = user;
	}

}
