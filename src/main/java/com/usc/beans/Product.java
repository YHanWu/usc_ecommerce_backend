package com.usc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_list")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;	
	
	@Column(name="item")
	@NotNull
	private String item;
	@Column(name="brand")
	private String brand;
	@Column(name="category")
	private String category;
	@Column(name="country")
	private String country;
	@Column(name="type")
	private String type;
	@Column(name="size")
	private String size;
	@Column(name="region")
	private String region;
	@Column(name="status")
	private String status;
	@Column(name="url")
	private String url;
	@Column(name="price")
	private String price;
	@Column(name="Taste")
	private String Taste;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getTaste() {
		return Taste;
	}
	public void setTaste(String taste) {
		Taste = taste;
	}
	public Product(Integer id, String item, String brand, String category, String country, String type, String size,
			String region, String status, String url, String price, String taste) {
		super();
		this.id = id;
		this.item = item;
		this.brand = brand;
		this.category = category;
		this.country = country;
		this.type = type;
		this.size = size;
		this.region = region;
		this.status = status;
		this.url = url;
		this.price = price;
		Taste = taste;
	}
	public Product() {
		super();
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", item=" + item + ", brand=" + brand + ", category=" + category + ", country="
				+ country + ", type=" + type + ", size=" + size + ", region=" + region + ", status=" + status + ", url="
				+ url + ", price=" + price + ", Taste=" + Taste + "]";
	}
	
	
	

}
