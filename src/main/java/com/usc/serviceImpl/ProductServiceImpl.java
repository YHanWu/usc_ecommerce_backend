package com.usc.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.usc.beans.Product;
import com.usc.repository.ProductRepository;
import com.usc.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> getProduct(String name, int page, int size) {
		System.out.print("fetching product for paging");

		// Page<Product> productResultPage =
		// productRepository.findAll(PageRequest.of(page, size));
		
		
		Page<Product> productResultPage = productRepository
				.findAll(PageRequest.of(page, size, Sort.by(name).ascending()));
		
		int totalPage = productResultPage.getTotalPages();

		if (productResultPage.hasContent()) {
			System.out.println(productResultPage.getContent());
			return productResultPage.getContent();
		} else {
			return new ArrayList<Product>();
		}

	}
	
	public void AddProduct(Product product) {
		productRepository.save(product);
	}
	
	public void updateProduct(Product product) {
		productRepository.save(product);
	}

	public void deleteProduct(Integer id) {
		productRepository.deleteById(id);
		
	}

}
