package com.usc.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;


import com.usc.beans.Order;
import com.usc.beans.OrderItem;
import com.usc.beans.Response;
import com.usc.beans.User;
import com.usc.repository.OrderRepository;
import com.usc.repository.UserRepository;

@Service
@Transactional
public class OrderServiceImpl {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemServiceImpl itemServiceImpl;
	
	@Autowired
	private OrderItemServiceImpl orderItemServiceImpl;

	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	public Optional<Order> getOrderById(Integer id) {
		return orderRepository.findById(id);
	}

	public Order orderCreate(Order order, Authentication authentication) {
		
		// get the user
		System.out.println(order.toString());
		User user = userRepository.findByUsername(authentication.getName());
		//System.out.println(user.toString());
		order.setUser(user);
		
		Order order1 = orderRepository.save(order);
		//System.out.println(order1.toString());
		
		List<OrderItem> orderItems = new ArrayList<>();
		for(OrderItem orderItem: order.getOrderitems()) {
			//tmpContents.add(contentService.createContent(book1.getId(),content));
			
			orderItems.add(orderItemServiceImpl.createItem(order1.getId(), orderItem));
			
		}
		
		order1.setOrderitems(orderItems);
		
		return order1;
	}

	public Optional<Order> getOrderByUserId(Integer id) {
		
		return null;
	}

}
