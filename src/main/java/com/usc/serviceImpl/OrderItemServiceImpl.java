package com.usc.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.beans.Item;
import com.usc.beans.Order;
import com.usc.beans.OrderItem;
import com.usc.repository.ItemRepository;
import com.usc.repository.OrderItemRepository;
import com.usc.repository.OrderRepository;
@Service
public class OrderItemServiceImpl {
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ItemRepository itemRepository;


	public List<OrderItem> getAllOrderItems() {
		return orderItemRepository.findAll();
	}


	public Optional<OrderItem> getOrderItemById(Integer id) {
		return orderItemRepository.findById(id);
	}
	
	public OrderItem createItem(Integer id, OrderItem orderItem) {
		Optional<Order> byId = orderRepository.findById(id);
	      
	    Order order = byId.get();
	    orderItem.setOrder(order);
//	    System.out.println("test!!!!!!!!!!!!!!!!!!!!!");
//	    System.out.println(orderItem.getItem().getName());

	    //OrderItem orderItem1 = orderItemRepository.save(orderItem);
	    OrderItem orderItem1;
	    System.out.println("test!!!!!!!!!!!!!!!!!!!!!");
	    System.out.println(orderItem.getItem().getId());
	    
	    Optional<Item> itemById = itemRepository.findById(orderItem.getItem().getId());
	    
	    Item item = itemById.get();
	    System.out.println(itemById.toString());
	    orderItem.setItem(item);
	    
	    orderItem1 = orderItemRepository.save(orderItem);

	    return orderItem1;
	}

}
