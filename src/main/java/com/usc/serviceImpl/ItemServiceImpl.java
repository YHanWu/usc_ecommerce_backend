package com.usc.serviceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.beans.Item;

import com.usc.beans.Product;
import com.usc.repository.ItemRepository;
import com.usc.repository.ProductRepository;

@Service
public class ItemServiceImpl {
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	ProductRepository productRepository;

	public void AddItem(Product product) {
		Item item = new Item();
		item.setBrand(product.getBrand());
		item.setCategory(product.getCategory());
		item.setCountry(product.getCountry());
		item.setName(product.getItem());
		item.setPrice(product.getPrice());
		item.setRegion(product.getRegion());
		item.setSize(product.getSize());
		item.setStatus(product.getStatus());
		item.setTaste(product.getTaste());
		item.setUrl(product.getUrl());
		item.setType(product.getType());
		item.setId(productRepository.findByItem(product.getItem()).getId());;
		System.out.println("item create!!!");
		System.out.println(item.toString());
		itemRepository.save(item);
		
	}

	public void updateItem(Product product) {
		Item item = new Item();
		item.setBrand(product.getBrand());
		item.setCategory(product.getCategory());
		item.setCountry(product.getCountry());
		item.setName(product.getItem());
		item.setPrice(product.getPrice());
		item.setRegion(product.getRegion());
		item.setSize(product.getSize());
		item.setStatus(product.getStatus());
		item.setTaste(product.getTaste());
		item.setUrl(product.getUrl());
		item.setType(product.getType());
		item.setId(productRepository.findByItem(product.getItem()).getId());
		//System.out.println("item create!!!");
		System.out.println(item.toString());
		itemRepository.save(item);
		
	}

	

}
