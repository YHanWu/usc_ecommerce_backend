package com.usc.service;

import java.util.List;

import com.usc.beans.Product;

public interface ProductService {

	List<Product> getProduct(String name, int page, int size);

}
