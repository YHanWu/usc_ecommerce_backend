package com.usc.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.usc.beans.Response;
import com.usc.beans.User;
import com.usc.beans.UserRole;
import com.usc.repository.UserRepository;

@Service
@Transactional
public class UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;
	
	public Response register(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword())); //password encoder將註冊密碼加密
		List<UserRole> profiles = new ArrayList<UserRole>();
		profiles.add(new UserRole(2));  // userprofile表裡 id=2 代表是general user 
		user.setUserRole(profiles);
		System.out.println(user);
		userRepository.save(user);  //把user存進database
		return new Response(true);
	}
	
	public Response registerAdm(User user) {
		System.out.println("input valid");
		System.out.println(user.toString());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		List<UserRole> profiles = new ArrayList<UserRole>();
		profiles.add(new UserRole(1)); // userprofile表裡 id=1 代表是admin
		user.setUserRole(profiles);
		System.out.println(user);
		userRepository.save(user);
		return new Response(true);
	}
	
//	public Response changePassword(User user, Authentication authentication) {
//		if(user.getUsername().equals(authentication.getName()) || isAdmin(authentication.getAuthorities())) {
//			User u = userRepository.findByUsername(user.getUsername());
//			u.setPassword(passwordEncoder.encode(user.getPassword()));
//			userRepository.save(u);
//		}else {
//			return new Response(false);
//		}
//		return new Response(true);
//	}
	
	public boolean isAdmin(Collection<? extends GrantedAuthority> profiles) {
		boolean isAdmin = false;
		for(GrantedAuthority profile : profiles) {
			if(profile.getAuthority().equals("ROLE_ADMIN")) {
				isAdmin = true;
			}
		}
		return isAdmin;
	}
	
	public Response deleteUser(int id) {
		if(userRepository.findById(id)!=null) {
			userRepository.deleteById(id);
			return new Response(true);
		}else {
			return new Response(false, "User is not found!");
		}
		
	}
	
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public Optional<User> getUserById(Integer id) {
		return userRepository.findById(id);
	}
	

}